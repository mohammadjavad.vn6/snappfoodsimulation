from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import BaseModel


class Coupon(BaseModel):
    code = models.CharField(max_length=50, unique=True)
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField()
    discount = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    max_value = models.IntegerField()
    active = models.BooleanField()

    def __str__(self):
        return self.code

    class Meta:
        db_table = 'coupon'
        verbose_name = _('coupon')
        verbose_name_plural = _('coupons')
