from django.db import models
from django.utils.translation import ugettext_lazy as _

from coupon.models import Coupon
from core.models import BaseModel


class Payment(BaseModel):
    CHOICES = (('credit', _('Credit')),
               ('payment_getway', _('Payment_getway')))

    is_paid = models.BooleanField(default=False)
    price = models.PositiveIntegerField(default=0)
    payment_type = models.CharField(choices=CHOICES, max_length=15)
    coupon = models.ForeignKey(Coupon, models.SET_NULL, 'payments', null=True)

    class Meta:
        db_table = 'payment'
        verbose_name = _('payment')
        verbose_name_plural = _('payments')
