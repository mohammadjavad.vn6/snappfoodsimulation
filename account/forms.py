import datetime
from django import forms
from django.contrib.auth import get_user_model, authenticate, login
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from account.authentication import PhoneNumberAuthBackend
from account.models import UserOTP

User = get_user_model()


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = User
        fields = ('email', 'phone_number')


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('email', 'phone_number')


class LoginWithEmailForm(forms.Form):
    email = forms.CharField(required=True, label=_('Enter Email'))
    password = forms.CharField(required=True, label=_('Enter Password'),
                               widget=forms.PasswordInput)

    def clean(self):
        if not "email" in self.cleaned_data:
            raise forms.ValidationError(_("Username Field is Required"))
        if not "password" in self.cleaned_data:
            raise forms.ValidationError(_("Password Field is Required"))
        user = User.objects.filter(
            email=self.cleaned_data["email"]).first()
        if user is None:
            raise forms.ValidationError(
                _("User with provided username does not exists"))
        if not user.check_password(self.cleaned_data["password"]):
            raise forms.ValidationError(_("Wrong password"))
        user = authenticate(**self.cleaned_data)
        if user is None:
            raise forms.ValidationError(
                _("Unable login with provided credentials"))
        self.cleaned_data["user"] = user

        return self.cleaned_data


class LoginWithPhoneNumberForm(forms.Form):
     phone_number = forms.CharField(required=True)

     def clean(self):
         user = User.objects.filter(
             phone_number=self.cleaned_data["phone_number"]).first()
         if user is None:
             raise forms.ValidationError(
                 _("User with provided phone number does not exists")
                )

         return self.cleaned_data


class VerifySMSCodeForm(forms.Form):
    sms_code = forms.CharField(
        required=True,  # widget=forms.PasswordInput,
        label="Enter the code which has sent"
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.backend_authenticate = "account.authentication.PhoneNumberAuthBackend"
        super().__init__(*args, **kwargs)

    def clean(self):
        user_code = UserOTP.objects.filter(code_type=UserOTP.LOGIN,
                                           phone_number=self.request.session[
                                               'phone_number']).last()

        user_code_expire_time = user_code.time_in_range(
            user_code.expire_time_start, user_code.expire_time_end,
            datetime.datetime.now()
        )
        if self.cleaned_data['sms_code'] != user_code.code:
            raise forms.ValidationError(_("This code is not True"))
        if user_code_expire_time is False:
            raise forms.ValidationError(_('This code is expired'))
        return self.cleaned_data

    def sign_in(self):
        user = PhoneNumberAuthBackend.authenticate(
            self, request=self.request,
            username=self.request.session["phone_number"]
        )
        login(self.request, user, backend=self.backend_authenticate)
        return user


class RememberEmailPassword(forms.Form):
    email = forms.EmailField(required=True)

    def clean(self):
        user = User.objects.filter(
            email=self.cleaned_data["email"]).first()
        if user is None:
            raise forms.ValidationError(
                _("User with provided email does not exists")
            )
        return self.cleaned_data


class VerifyRememberEmailPasswordSMS(forms.Form):
    code = forms.CharField(required=True, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.backend_authenticate = "account.authentication.PhoneNumberAuthBackend"
        super().__init__(*args, **kwargs)

    def clean(self):
        user_code = UserOTP.objects.filter(code_type=UserOTP.EMAIL,
                                           email=self.request.session[
                                               'email']).last()

        user_code_expire_time = user_code.time_in_range(
            user_code.expire_time_start, user_code.expire_time_end,
            datetime.datetime.now()
        )
        if self.cleaned_data['code'] != user_code.code:
            raise forms.ValidationError(_("This code is not True"))
        if user_code_expire_time is False:
            raise forms.ValidationError(_('This code is expired'))
        return self.cleaned_data

    def sign_in(self):
        user = PhoneNumberAuthBackend.authenticate(
            self, request=self.request,
            email=self.request.session["email"]
        )
        login(self.request, user, backend=self.backend_authenticate)
        return user


class RegisterForm(forms.ModelForm):
    password = forms.CharField(label=_('Password'), widget=forms.PasswordInput, required=True)
    password2 = forms.CharField(label=_('Repeat Password'), widget=forms.PasswordInput, required=True)

    class Meta:
        model = User
        fields = ('name', 'email', 'phone_number')

    def clean_password2(self):
        if self.cleaned_data['password'] != self.cleaned_data['password2']:
            raise forms.ValidationError(_('Passwords do\'nt match'))
        return self.cleaned_data['password2']


class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(required=True, label=_('Old password'), widget=forms.PasswordInput)
    new_password = forms.CharField(required=True, label=_('New password'), widget=forms.PasswordInput)
    r_new_password = forms.CharField(required=True, label=_('Repeat new password '), widget=forms.PasswordInput)

    def clean_r_new_password(self):
        if self.cleaned_data['new_password'] != self.cleaned_data['r_new_password']:
            raise forms.ValidationError(_('Passwords do\'nt match'))
        return self.cleaned_data['r_new_password']


class PersonalInfoForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('name', 'email', 'phone_number')


