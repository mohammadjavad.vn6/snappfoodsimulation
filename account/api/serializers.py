import jwt
from django.conf import settings
from django.contrib.auth import authenticate
from rest_framework import serializers
from rest_framework_jwt.serializers import jwt_payload_handler

from account.managers import CustomUserManager
from account.models import UserAddress, User
from comment.api.serializers import CommentProfileUserSerializer
from order.api.serializers import OrderSerializer


class UserAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAddress
        fields = ('lat', 'long')


class UserProfileSerializer(serializers.ModelSerializer):
    addresses = UserAddressSerializer(many=True)
    comments = CommentProfileUserSerializer(many=True)
    orders = OrderSerializer(many=True)

    class Meta:
        model = User
        fields = (
            'username', 'email', 'name', 'credit', 'phone_number', 'comments',
            'addresses', 'orders'
        )


class UserLightSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
             'name', 'created_time'
        )


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True, max_length=128)
    new_password = serializers.CharField(required=True, max_length=128)
    new_password_repeat = serializers.CharField(required=True, max_length=128)


class UserLoginWithEmail(serializers.ModelSerializer):
    token = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ('email', 'password', 'token')

    def get_token(self, obj):
        payload = jwt_payload_handler(obj)
        token = jwt.encode(payload, settings.SECRET_KEY)
        return token


class SignUpSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ('name', 'email', 'phone_number', 'password', 'token')

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    def get_token(self, obj):
        payload = jwt_payload_handler(obj)
        token = jwt.encode(payload, settings.SECRET_KEY)
        return token