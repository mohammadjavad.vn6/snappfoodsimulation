import jwt
from django.conf import settings
from django.contrib.auth import authenticate
from django.urls import reverse
from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.serializers import jwt_payload_handler

from account.api.serializers import UserAddressSerializer, \
    UserProfileSerializer, ChangePasswordSerializer, UserLoginWithEmail, \
    SignUpSerializer
from account.managers import CustomUserManager
from account.models import UserAddress, User


class ProfileAPIView(generics.RetrieveAPIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, )
    serializer_class = UserProfileSerializer
    queryset = User.objects.all()
    # lookup_field = ('pk', )

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, pk=self.request.user.pk)
        return obj


class RegisterView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = SignUpSerializer


class LoginAPIView(generics.GenericAPIView):
    serializer_class = UserLoginWithEmail
    permission_classes = (AllowAny, )

    def get_object(self):
        queryset = User.objects.filter(
            email=self.request.data['email']
        ).first()
        return queryset

    def post(self, request):
        if request.method == "POST":
            user = self.get_object()
            serializer = self.get_serializer(user)
            if user is None:
                error = {"error": "User With provided data does not exist"}
                return Response(data=error, status=status.HTTP_403_FORBIDDEN)
            return Response(data=serializer.data, status=status.HTTP_200_OK)


class UserAddressCreateAPIView(generics.CreateAPIView):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, )
    serializer_class = UserAddressSerializer
    queryset = UserAddress.objects.all()

    def create(self, request, *args, **kwargs):
        super().create(request, *args, **kwargs)
        return Response(data={"message": "Your address saved"})

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ChangePassword(generics.UpdateAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ChangePasswordSerializer
    queryset = User.objects.all()

    def get_object(self):
        return self.request.user

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            if not self.object.check_password(serializer.data.get('old_password')):
                return Response({"old_password": ["Wrong password."]},
                                status=status.HTTP_400_BAD_REQUEST)
            self.object.set_password(serializer.data.get('new_password'))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password changed successfully',
                'data': []
            }
            return Response(response)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)