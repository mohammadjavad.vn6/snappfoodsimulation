from .views import UserAddressCreateAPIView, ProfileAPIView, ChangePassword, \
    LoginAPIView, RegisterView
from django.urls import path


app_name = 'api_account'

urlpatterns = [
    path('profile/', ProfileAPIView.as_view(), name='profile_api'),
    path('address/', UserAddressCreateAPIView.as_view(), name='address_api'),
    path('change-password/', ChangePassword.as_view(), name='change_password'),
    path('login-email/', LoginAPIView.as_view(), name='login_email'),
    path('register/', RegisterView.as_view(), name='register_api')
]