import datetime

from django.contrib import messages
from django.contrib.auth import get_user_model, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LogoutView
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.utils.crypto import get_random_string
from django.utils.decorators import method_decorator
from django.views.generic import FormView, DetailView
from django.views.generic.base import TemplateView, View, RedirectView

from account.authentication import PhoneNumberAuthBackend
from account.forms import RegisterForm, ChangePasswordForm, \
    PersonalInfoForm, LoginWithEmailForm, LoginWithPhoneNumberForm, \
    VerifySMSCodeForm, RememberEmailPassword, VerifyRememberEmailPasswordSMS
from account.models import UserOTP
from account.tasks import send_verification_sms, send_remember_password_code

User = get_user_model()


class LoginWithEmail(FormView):
    form_class = LoginWithEmailForm
    template_name = 'account/login.html'
    success_url = reverse_lazy('account:profile')

    def form_valid(self, form):
        login(self.request, form.cleaned_data["user"])
        return super().form_valid(form)


class LoginWithPhone(FormView):
    form_class = LoginWithPhoneNumberForm
    template_name = "account/sms_login.html"

    def form_valid(self, form):
        phone_number = form.cleaned_data["phone_number"]
        code = get_random_string(length=6, allowed_chars="0123456789abcdef")
        OTP_code = UserOTP.objects.create(
            code=code, code_type=UserOTP.LOGIN,
            expire_time_end=(datetime.datetime.now() + datetime.timedelta(
                minutes=1)),
            phone_number=phone_number
        )
        self.request.session["phone_number"] = phone_number
        self.request.session["sms_verify_code"] = code
        send_verification_sms.delay(phone_number, code)
        return HttpResponseRedirect(reverse('account:verify_code'))


class VerifySMSCode(FormView):
    form_class = VerifySMSCodeForm
    template_name = 'account/verify_sms_code.html'
    success_url = reverse_lazy('account:profile')
    backend_authenticate = "account.authentication.PhoneNumberAuthBackend"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'request': self.request
        })
        return kwargs

    def form_valid(self, form):
        form.sign_in()
        del self.request.session['sms_verify_code']
        del self.request.session["phone_number"]
        return super().form_valid(form)


class RepeatLoginVerificationCode(RedirectView):
    url = reverse_lazy('account:verify_code')

    def get(self, request, *args, **kwargs):
        code = get_random_string(length=6, allowed_chars="0123456789abcdef")
        OTP = UserOTP.objects.create(
            code=code,
            expire_time_end=(datetime.datetime.now() + datetime.timedelta(
                minutes=1)), code_type=UserOTP.LOGIN,
            phone_number=request.session['phone_number']
        )
        send_verification_sms.delay(request.session['phone_number'], code)
        return super().get(request, *args, **kwargs)


class RememberEmailPasswordView(FormView):
    form_class = RememberEmailPassword
    template_name = "account/forgot_email_password.html"

    def form_valid(self, form):
        email = form.cleaned_data["email"]
        code = get_random_string(length=6, allowed_chars="0123456789abcdef")
        send_remember_password_code.delay(email, code)
        self.request.session['email'] = email
        OTP_code = UserOTP.objects.create(
            code=code, code_type=UserOTP.EMAIL,
            expire_time_end=(datetime.datetime.now() + datetime.timedelta(
                minutes=1)),
            email=email
        )
        return HttpResponseRedirect(reverse('account:check_remember_password'))


class RememberVerifyEmailCode(FormView):
    form_class = VerifyRememberEmailPasswordSMS
    template_name = 'account/verify_remember_code.html'
    success_url = reverse_lazy('account:profile')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'request': self.request
        })
        return kwargs

    def form_valid(self, form):
        form.sign_in()
        del self.request.session["email"]
        return super().form_valid(form)


class RepeatRememberEmailPassword(RedirectView):
    url = reverse_lazy('account:check_remember_password')

    def get(self, request, *args, **kwargs):
        code = get_random_string(length=6, allowed_chars="0123456789abcdef")
        OTP = UserOTP.objects.create(
            code=code,
            expire_time_end=(datetime.datetime.now() + datetime.timedelta(
                minutes=1)), code_type=UserOTP.EMAIL,
            email=request.session['email']
        )
        send_remember_password_code.delay(request.session['email'], code)
        return super().get(request, *args, **kwargs)


class Logout(LogoutView):
    template_name = 'account/home.html'


def register(request):
    if request.method == 'POST':
        form = RegisterForm(data=request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.set_password(form.cleaned_data['password'])
            new_user.save()
            return redirect('account:profile')
    else:
        form = RegisterForm()
    return render(request, 'account/register.html', {'form': form})


@method_decorator(login_required, name='dispatch')
class Profile(View):
    def get_object(self):
        user = User.objects.get_user_data(self.request.user.id)
        return user

    def get(self, request):
        if request.method == "GET":
            context = dict()
            all_data = self.get_object()
            change_password_form = ChangePasswordForm()
            context['change_password_form'] = change_password_form
            personal_info_form = PersonalInfoForm(instance=request.user)
            context['personal_info_form'] = personal_info_form
            context['orders'] = all_data.orders.all()
            context['comments'] = all_data.comments.all()
            context['addresses'] = all_data.addresses.all()
            return render(request, 'account/profile.html', context)


def change_password(request):
    context = dict()
    if request.method == 'GET':
        change_password_form = ChangePasswordForm()
        context['change_password_form'] = change_password_form
        context['tab'] = 'password'

    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            if not request.user.check_password(form.cleaned_data['old_password']):
                messages.error(request, 'Incorrect password')
            request.user.set_password(form.cleaned_data['new_password'])
            request.user.save()
            messages.success(request, 'Your password was successfully updated!')
        else:
            messages.error(request, 'Please correct the error below.')
    return render(request, 'account/profile.html', context)


class HomeView(TemplateView):
    template_name = 'account/home.html'