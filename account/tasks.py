from celery.task import task
from django.conf.global_settings import EMAIL_HOST_USER
from django.core.mail import send_mail
from kavenegar import *
from snap_food import local_config
import requests


@task
def send_verification_sms(phone_number, code):
    url = 'https://api.kavenegar.com/v1/{}/sms/send.json'.format(local_config.API_KEY)
    params = {
             'receptor': '{}'.format(phone_number),
             'message': 'Your verification code is: {}'.format(code),
              }
    response = requests.post(url=url, data=params)
    print(response)
    print('*'*5, 'code is: ', code, '*'*5)
    # try:
    #     api = KavenegarAPI(local_config.API_KEY)
    #     params = {
    #         'sender': local_config.SMS_SENDER,  # optional
    #         'receptor': '{}'.format(phone_number),  # multiple mobile number, split by comma
    #         'message': 'your verification code is {}'.format(code),
    #     }
    #     response = api.sms_send(params)
    #     print(response)
    # except APIException as e:
    #     print(e)
    # except HTTPException as e:
    #     print(e)

@task
def send_remember_password_code(email, code):
    send_mail(
        "Recovery Email Password",
        'Hi, Your recovery password is: {}'.format(code),
        EMAIL_HOST_USER,
        ['{}'.format(email)],
        fail_silently=False
    )
    print('*' * 5, 'code is: ', code, '*' * 5)