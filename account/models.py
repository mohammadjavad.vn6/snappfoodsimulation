import datetime

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import BaseModel
from .managers import CustomUserManager


class UserAddress(BaseModel):
    #scope = models.CharField(max_length=250, verbose_name=_('scope'))
    #location = models.CharField(max_length=250, verbose_name=_('location'))
    #tag = models.CharField(max_length=250, verbose_name=_('tag'))
    lat = models.DecimalField(verbose_name=_("latitude"), max_digits=8, decimal_places=4, null=True)
    long = models.DecimalField(verbose_name=_("longitude"), max_digits=8, decimal_places=4, null=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, 'addresses')

    class Meta:
        db_table = "user_address"
        verbose_name = _('user_address')
        verbose_name_plural = _('user_addresses')


class User(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)
    name = models.CharField(max_length=80, default='', verbose_name=_('name'))
    credit = models.PositiveIntegerField(default=0, verbose_name=_('credit'))
    phone_regex = RegexValidator(
        regex=r'(\+98|0)?9\d{9}',
        message=_("Phone number must be entered in the format: '+989999999999' "
                  "or '09999999999'. Up to 11 digits allowed.")
    )
    phone_number = models.CharField(
        max_length=11, validators=[phone_regex], verbose_name=_('phone_number')
    )
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def __str__(self):
        return self.name


class UserOTP(models.Model):
    SIGNUP = 1
    LOGIN = 2
    EMAIL = 3
    CODE_TYPE_CHOICES = (
        (SIGNUP, "sign_up"),
        (LOGIN, 'login'),
        (EMAIL, 'email')
    )
    code = models.CharField(verbose_name=_('code'), max_length=6)
    expire_time_start = models.DateTimeField(verbose_name=_("start of expire time"), default=datetime.datetime.now, null=True)
    expire_time_end = models.DateTimeField(_("end of expire time"), null=True)
    code_type = models.IntegerField(_("code type"), choices=CODE_TYPE_CHOICES, null=True)
    phone_number = models.CharField(_('phone number'), max_length=11, null=True)
    email = models.EmailField(verbose_name=_('email'), null=True)

    def time_in_range(self, start, end, x):
        """Return true if x is in the range [start, end]"""
        if start <= end:
            return start <= x <= end
        else:
            return start <= x or x <= end