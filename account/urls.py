from django.urls import path
from . import views
app_name = 'account'
urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('login-email/', views.LoginWithEmail.as_view(), name='login_email'),
    path('login-phone', views.LoginWithPhone.as_view(), name='login_phone'),
    path('check-sms', views.VerifySMSCode.as_view(), name='verify_code'),
    path("repeat-register-verification-code/", views.RepeatLoginVerificationCode.as_view(), name='login_otp'),
    path('repeat-remember-email-password-code/', views.RepeatRememberEmailPassword.as_view(), name='email_otp'),
    path('forgot-password', views.RememberEmailPasswordView.as_view(), name='forgot_password'),
    path('verify-remember-password', views.RememberVerifyEmailCode.as_view(), name='check_remember_password'),
    path('logout', views.Logout.as_view(), name='logout'),
    path('register/', views.register, name='register'),
    path('profile/', views.Profile.as_view(), name='profile'),
    path('profile/password_change/', views.change_password, name='password_change'),
]
