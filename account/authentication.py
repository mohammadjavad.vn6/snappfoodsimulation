from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

User = get_user_model()


class PhoneNumberAuthBackend(ModelBackend):
    """
    Authenticate using an phone number.
    """

    def authenticate(self, request, username=None, email=None, **kwargs):
        if email is None:
            try:
                user = User.objects.filter(phone_number=username).first()
                return user
            except User.DoesNotExist:
                return None
        if username is None:
            try:
                user = User.objects.filter(email=email).first()
                return user
            except User.DoesNotExist:
                return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
