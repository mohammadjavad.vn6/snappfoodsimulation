from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('', include('account.urls', namespace='account')),
    path('restaurants/', include('restaurant.urls', namespace='restaurants')),
    path('order/', include('order.urls', namespace='order')),
    path('profile/comment/', include('comment.urls', namespace='comment')),
    path('api-token-auth/', obtain_jwt_token),
    path('api/account/', include('account.api.urls', namespace='api_account')),
    path('api/restaurants/', include('restaurant.api.urls', namespace='api_restaurant')),
    path('api/order/', include('order.api.urls', namespace='api_order'))

)
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += path('__debug__/', include(debug_toolbar.urls)),
