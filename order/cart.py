from django.conf import settings


class Cart:
    def __init__(self, request):
        # store current session to a variable for using in methos in this class
        self.session = request.session
        # get cart dictionary from session
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart

    def add_food(self, food):
        food_id = str(food.id)
        self.cart[food_id] = {'food_id': food_id,
                              'food_title': food.title,
                              'quantity': 1,
                              'price': food.price}
        self.save()

    def increase_food_count(self, food):
        food_id = str(food.id)
        self.cart[food_id]['quantity'] += 1
        self.save()

    def decrease_food_count(self, food):
        food_id = str(food.id)
        self.cart[food_id]['quantity'] -= 1
        self.save()

    def remove(self, food):
        food_id = str(food.id)
        del self.cart[food_id]
        self.save()

    def save(self):
        self.session.modified = True

    def __len__(self):
        return sum(food['quantity'] for food in self.cart.values())

    def __iter__(self):
        for food in self.cart.values():
            yield food

    def __contains__(self, food):
        food_id = str(food.id)
        if food_id in self.cart.keys():
            return True

    def get_food_quantity(self, food):
        food_id = str(food.id)
        return self.cart[food_id]['quantity']

    @property
    def get_total_price(self):
        return sum(food['price'] * food['quantity'] for food in self.cart.values())

    @property
    def get_items_count(self):
        return sum(food['quantity'] for food in self.cart.values())
