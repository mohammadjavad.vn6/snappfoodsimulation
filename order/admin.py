from django.contrib import admin

from order.models import Order, OrderFood


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(OrderFood)
class OrderAdmin(admin.ModelAdmin):
    pass
