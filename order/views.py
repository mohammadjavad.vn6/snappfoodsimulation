from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_POST

from order.decorators import ajax_required
from restaurant.models import Food
from .cart import Cart
from .forms import CheckoutForm
from .models import OrderFood, Order


@ajax_required
def add_order(request):
    data = dict()
    foods = list()
    cart = Cart(request)

    food_id = request.POST.get('food_id')
    food = Food.objects.get(id=food_id)

    if food in cart:
        cart.increase_food_count(food)

    else:
        cart.add_food(food)

    for item in cart:
        foods.append(item)

    data['foods'] = foods
    data['total_price'] = cart.get_total_price
    data['order_items_count'] = cart.get_items_count
    return JsonResponse(data)


@ajax_required
def increase_count(request):
    data = dict()
    food_id = request.POST.get('food_id')
    food = Food.objects.get(id=food_id)
    cart = Cart(request)
    cart.increase_food_count(food)
    data['quantity'] = cart.get_food_quantity(food)
    data['total_price'] = cart.get_total_price
    data['order_items_count'] = cart.get_items_count
    return JsonResponse(data)


@ajax_required
def decrease_count(request):
    data = dict()
    empty = False
    food_id = request.POST.get('food_id')
    food = Food.objects.get(id=food_id)
    cart = Cart(request)
    cart.decrease_food_count(food)
    quantity = cart.get_food_quantity(food)
    if quantity == 0:
        empty = True
        cart.remove(food)
    data['quantity'] = quantity
    data['empty'] = empty
    data['total_price'] = cart.get_total_price
    data['order_items_count'] = cart.get_items_count
    return JsonResponse(data)


@require_POST
@login_required
def check_out(request):
    data = dict()
    foods = list()
    cart = Cart(request)
    form = CheckoutForm(request.POST)
    if form.is_valid():
        description = form.cleaned_data['description']
        new_order = Order.objects.create(user=request.user)
        for item in cart:
            foods.append(item)
            food = Food.objects.get(id=item['food_id'])
            new_order_item = OrderFood(order=new_order, food=food, count=item['quantity'])
            new_order_item.save()

        data['user_name'] = request.user.name
        data['user_phone_number'] = request.user.phone_number
        data['user_email'] = request.user.email
        data['addresses'] = request.user.addresses.all()
        data['total_price'] = cart.get_total_price
        data['order_items_count'] = cart.get_items_count
        data['order_items'] = foods

    return render(request, 'order/payment.html', data)
