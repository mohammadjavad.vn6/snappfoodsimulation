# Generated by Django 2.2 on 2020-05-03 18:11

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0005_auto_20200503_1542'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='uuid',
            field=models.UUIDField(default=uuid.UUID('f3687179-176b-445b-8598-e67e63899d1a'), verbose_name='order_number'),
        ),
    ]
