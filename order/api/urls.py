from django.urls import path
from . import views

app_name = 'api_order'
urlpatterns = [
    path('', views.CreateOrderAPIView.as_view(), name='add_order_api'),
]