from rest_framework import serializers

from comment.api.serializers import CommentProfileUserSerializer, \
    OrderCommentSerializer
from order.models import Order, OrderFood
from restaurant.api.serializers import RestaurantLightSerializer, \
   FoodLightSerializer


class OrderSerializer(serializers.ModelSerializer):
    comment = CommentProfileUserSerializer()
    restaurant = RestaurantLightSerializer()
    foods = FoodLightSerializer(many=True)

    class Meta:
        model = Order
        fields = ('restaurant', 'foods', 'payment', 'active', 'comment', )


class OrderFoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderFood
        fields = ('food', 'order', 'count')


class CreateOrderSerializer(serializers.ModelSerializer):
    foods = OrderFoodSerializer(many=True)
    comment = OrderCommentSerializer()

    class Meta:
        model = Order
        fields = (
            'user', 'restaurant', 'foods', 'payment',
            'active', 'comment',
        )
        extra_kwargs = {'user': {'read_only': True}}

    def create(self, validated_data):
        food = validated_data.pop('foods')
        comment = validated_data.pop('comment')
        if comment is not None:
            comment_instance = OrderCommentSerializer(data=comment)
            if comment_instance.is_valid():
                comment_instance.save(
                    user=validated_data.get('user'),
                    restaurant=validated_data.get('restaurant')
                )
        order_instance = Order.objects.create(
            **validated_data, comment=comment_instance.instance
        )
        for food_order in food:
            serializer_order_food = OrderFoodSerializer(data={"food": food_order['food'].pk, "count": food_order['count']})
            if serializer_order_food.is_valid():
                serializer_order_food.save(order=order_instance)
        return order_instance
