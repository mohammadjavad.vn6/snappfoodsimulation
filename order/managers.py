from django.db import models
from django.db.models import Sum, F


class OrderFoodManager(models.Manager):
    def get_order_price(self, order):
        return self.filter(order=order) \
            .aggregate(order_price=Sum(F('food__price') * F('count')))['order_price']

    def get_total_sales_price(self):
        return self.aggregate(total_sales_price=Sum(F('food__price') * F('count')))['total_sales_price']
