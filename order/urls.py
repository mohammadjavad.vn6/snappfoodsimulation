from django.urls import path
from . import views

app_name = 'order'
urlpatterns = [
    path('order', views.add_order, name='add_order'),
    path('increase', views.increase_count, name='increase_count'),
    path('decrease', views.decrease_count, name='decrease_count'),
    path('checkout', views.check_out, name='checkout'),
]
