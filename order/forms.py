from django import forms


class CheckoutForm(forms.Form):
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'rows': '5'}),label='')
