from uuid import uuid4

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from comment.models import Comment
from core.models import BaseModel
from order.managers import OrderFoodManager
from payment.models import Payment
from restaurant.models import Restaurant, Food


class OrderFood(BaseModel):
    food = models.ForeignKey(Food, models.SET_NULL, null=True)
    order = models.ForeignKey('order', models.SET_NULL, null=True)
    count = models.PositiveIntegerField(default=1)

    objects = models.Manager()
    rows = OrderFoodManager()

    class Meta:
        db_table = 'order_food'
        verbose_name = _('order_food')

    @property
    def get_price(self):
        return self.food.price * self.count


class Order(BaseModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, 'orders')
    restaurant = models.ForeignKey(Restaurant, models.CASCADE, 'orders')
    uuid = models.UUIDField(default=uuid4(), verbose_name=_('order_number'))
    foods = models.ManyToManyField(Food, through=OrderFood, related_name='orders')
    payment = models.OneToOneField(Payment, models.SET_NULL, null=True, blank=True)
    active = models.BooleanField(default=True)
    comment = models.OneToOneField(Comment, models.SET_NULL, null=True)

    class Meta:
        db_table = 'order'
        verbose_name = _('order')
        verbose_name_plural = _('orders')

    def __str__(self):
        return str(self.uuid)
