from django.db import models
from .constants import RESTAURANT, COFFEE_SHOP, CONFECTIONERY, SUPER_MARKET


class RestaurantManager(models.Manager):

    def get_restaurants(self, service_type, sort, filters, food_types):
        self.queryset = super().get_queryset()
        if service_type is not None:
            self.__filter_by_service_type(service_type)
        if sort is not None:
            self.__sort(sort)
        if filters is not None:
            self.__filter(filters)
        if food_types is not None:
            self.__filter_by_food_type(food_types)
        return self.queryset

    def __filter_by_service_type(self, service_type):
        if service_type == 'all':
            pass
        elif service_type == 'restaurant':
            self.queryset = self.queryset.filter(service_type=RESTAURANT)
        elif service_type == 'coffee':
            self.queryset = self.queryset.filter(service_type=COFFEE_SHOP)
        elif service_type == 'confectionery':
            self.queryset = self.queryset.filter(service_type=CONFECTIONERY)
        elif service_type == 'super_market':
            self.queryset = self.queryset.filter(service_type=SUPER_MARKET)

    def __sort(self, sort):
        if sort == 'newest':
            self.queryset = self.queryset.order_by('-created_time')
        elif sort == 'speed':
            self.queryset = self.queryset.order_by('time_to_delivery')
        elif sort == 'max_rate':
            # TODO
            self.queryset = self.queryset

    def __filter(self, filters):
        # TODO
        return self.queryset

    def __filter_by_food_type(self, food_types):
        # TODO better performance
        ids = list()
        for restaurant in self.queryset:
            for category in restaurant.categories.all():
                if category.id in food_types:
                    ids.append(restaurant.id)

        return self.queryset.filter(pk__in=ids)
