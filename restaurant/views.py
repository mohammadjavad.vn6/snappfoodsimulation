from django.shortcuts import render, get_object_or_404

from order.forms import CheckoutForm
from .models import Restaurant, Category


def restaurants(request):
    params = dict(request.GET)
    service_type = request.GET.get('service_type')
    ordering = request.GET.get('ordering')
    filters = params.get('filters')
    food_type = params.get('food_type')
    context = dict()
    context['categories'] = Category.objects.all()
    try:
        context['restaurants'] = Restaurant.objects.get_restaurants(service_type, ordering, filters, food_type)
    except:
        context['restaurants'] = None
    return render(request, 'restaurant/restaurants.html', context)


def restaurant(request, slug):
    context = dict()
    context['restaurant'] = get_object_or_404(Restaurant, slug=slug)
    context['comments'] = context['restaurant'].get_comments()
    context['average_rate'] = context['restaurant'].get_average_rate()
    context['food_quality_average'] = context['restaurant'].get_food_quality_average()
    context['packaging_quality_average'] = context['restaurant'].get_packaging_quality_average()
    context['speed_transfer_average'] = context['restaurant'].get_speed_transfer_average()
    context['agent_average'] = context['restaurant'].get_agent_average()
    context['form'] = CheckoutForm()
    return render(request, 'restaurant/restaurant.html', context)
