from rest_framework.pagination import LimitOffsetPagination


class RestaurantListPagination(LimitOffsetPagination):
    max_limit = 10
    default_limit = 6