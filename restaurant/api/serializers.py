from rest_framework import serializers

from order.models import OrderFood
from restaurant.models import Restaurant, Food, Category, RestaurantCategory


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('title',)


class FoodSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Food
        fields = ('title', 'price', 'image', 'description', 'active', 'category')


class RestaurantSerializer(serializers.ModelSerializer):
    from comment.api.serializers import CommentRestaurantSerializer
    average_rate = serializers.FloatField(source='get_average_rate')
    agent_average = serializers.SerializerMethodField()
    food_quality_average = serializers.SerializerMethodField()
    packaging_quality_average = serializers.SerializerMethodField()
    speed_transfer_average = serializers.SerializerMethodField()
    #comment = serializers.SerializerMethodField()
    comments = CommentRestaurantSerializer(many=True)
    #categories = CategorySerializer(many=True)
    foods = FoodSerializer(many=True)

    class Meta:
        model = Restaurant
        fields = (
            'name', 'slug', 'orders_type', 'working_times', 'service_scope',
            'minimum_order', 'rate', 'time_to_delivery', 'comments',
            'service_type', 'lat', 'long', 'service_area', 'foods',
            'average_rate', 'agent_average', 'food_quality_average',
            'packaging_quality_average', 'speed_transfer_average',
        )

    # def get_comment(self, obj):
    #     cm = obj.get_comments()
    #     return cm

    def get_agent_average(self, obj):
        return obj.get_agent_average()

    def get_food_quality_average(self, obj):
        return obj.get_food_quality_average()

    def get_packaging_quality_average(self, obj):
        return obj.get_packaging_quality_average()

    def get_speed_transfer_average(self, obj):
        return obj.get_speed_transfer_average()


class RestaurantLightSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = (
            'name', 'slug', 'orders_type', 'rate', 'image',
        )


class FoodLightSerializer(serializers.ModelSerializer):
    class Meta:
        model = Food
        fields = ('title', 'price', 'image')


class RestaurantListSerializer(serializers.ModelSerializer):
    comments = serializers.SerializerMethodField()

    class Meta:
        model = Restaurant
        fields = (
            'name', 'slug', 'orders_type', 'rate', 'time_to_delivery', 'image',
            'comments', 'service_type'
        )

    def get_comments(self, obj):
         count = obj.comments.count()
         return count