from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.filters import SearchFilter
from rest_framework.permissions import AllowAny

from restaurant.api.paginations import RestaurantListPagination
from restaurant.api.serializers import RestaurantListSerializer, \
    RestaurantSerializer
from restaurant.models import Restaurant


class RestaurantListAPIView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = RestaurantListSerializer
    pagination_class = RestaurantListPagination
    queryset = Restaurant.objects.prefetch_related('comments').all()
    filter_backends = (SearchFilter, DjangoFilterBackend)
    search_fields = ('name',)
    filterset_fields = ('service_type',)


class RestaurantRetrieveAPIView(generics.RetrieveAPIView):
    permission_classes = (AllowAny,)
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()

    def get_object(self):
        obj = Restaurant.objects.prefetch_related('comments', 'foods').filter(slug=self.kwargs['slug']).first()
        return obj
