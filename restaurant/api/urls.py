from django.urls import path
from . import views

app_name = 'api_restaurant'
urlpatterns = [
    path('', views.RestaurantListAPIView.as_view(), name='restaurant_list_api_view'),
    path('<slug:slug>/', views.RestaurantRetrieveAPIView.as_view(), name='restaurant_retrieve_api_view')
]
