function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function addFood(restaurant_id, food_id) {

    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.post("http://127.0.0.1:8000/order/order",
        {
            food_id: food_id
        },
        function (data) {
            let foods = data.foods;
            document.getElementById('basket').innerHTML = "";
            for (let i = 0; i < foods.length; i++) {
                $('#basket').append(`<li class="list-group-item " id="li_${foods[i].food_id}">
                        <div class="row mb-2">
                            <div class="col">
                                <span>${foods[i].food_title}</span>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-6">
                                <span>${foods[i].price}</span>
                                <span>&nbspتومن</span>
                            </div>
                            <div class="col-4 offset-2">
                                <span onclick="increaseCount(${foods[i].food_id})">+&nbsp</span>
                                <span id="food_${foods[i].food_id}">${foods[i].quantity}&nbsp</span>
                                <span onclick="decreaseCount(${foods[i].food_id})">-</span>
                            </div>
                        </div>
                    </li>`)
            }

            $('#order_items_count').text(data.order_items_count);
            $('#total_price').text(data.total_price);
        }
    )
}

function increaseCount(food_id) {
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.post("http://127.0.0.1:8000/order/increase",
        {
            food_id: food_id
        },
        function (data) {
            $(`#food_${food_id}`).text(data.quantity);
            $('#order_items_count').text(data.order_items_count);
            $('#total_price').text(data.total_price);
        }
    )

}

function decreaseCount(food_id) {
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.post("http://127.0.0.1:8000/order/decrease",
        {
            food_id: food_id
        },
        function (data) {
            if (data.empty) {
                $(`#li_${food_id}`).remove()
            }
            $(`#food_${food_id}`).text(data.quantity);
            $('#order_items_count').text(data.order_items_count);
            $('#total_price').text(data.total_price);
        }
    )
}

function getComments(restaurantId) {
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.post("http://127.0.0.1:8000/restaurant/comments",
        {
            restaurantID: restaurantID
        },
        function (data) {

        }
    );

}