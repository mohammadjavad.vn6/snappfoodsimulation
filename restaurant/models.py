from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Avg, F, Sum
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from core.models import BaseModel
from .constants import SERVICE_CHOICES, TIME_TO_DELIVERY_CHOICES
from .managers import RestaurantManager


class Restaurant(BaseModel):
    name = models.CharField(max_length=250, verbose_name=_('name'))
    slug = models.SlugField(unique=True)
    orders_type = models.CharField(max_length=250, verbose_name=_('orders_type'))
    working_times = models.TextField(verbose_name=_('working_times'))
    service_scope = models.TextField(verbose_name=_('service_scope'))
    minimum_order = models.PositiveIntegerField(verbose_name=_('minimum_order'), default=0)
    rate = models.FloatField(verbose_name=_('rate'), null=True)
    time_to_delivery = models.IntegerField(choices=TIME_TO_DELIVERY_CHOICES, verbose_name=_('time_to_delivery'))
    image = models.ImageField(verbose_name=_('image'), upload_to='images/restaurant')
    service_type = models.IntegerField('service type', choices=SERVICE_CHOICES)
    # address fields
    # scope = models.CharField(max_length=250, verbose_name=_('scope'))
    # location = models.CharField(max_length=250, verbose_name=_('location'))
    # tag = models.CharField(max_length=250, verbose_name=_('tag'))
    lat = models.DecimalField(verbose_name=_("latitude"), max_digits=8,
                              decimal_places=4, null=True)
    long = models.DecimalField(verbose_name=_("longitude"), max_digits=8,
                               decimal_places=4, null=True)
    service_area = models.SmallIntegerField(_("service area"), null=True)

    categories = models.ManyToManyField('Category', through='RestaurantCategory', related_name='restaurants')

    objects = RestaurantManager()

    class Meta:
        db_table = 'restaurant'
        verbose_name = _('restaurant')
        verbose_name_plural = _('restaurants')

    def get_absolute_url(self):
        return reverse('restaurants:restaurant', args=[self.slug])

    def __str__(self):
        return self.name

    def get_comments(self):
        return self.comments.all() \
            .annotate(
            average_rate=(Sum(
                F('food_quality') + F('packaging_quality') + F('speed_transfer') + F('agent')) / 4))

    def get_average_rate(self):
        return self.get_comments().aggregate(total_rate=Avg('average_rate'))['total_rate']

    def get_agent_average(self):
        try:
            result = self.get_comments().aggregate(total_rate=Avg('average_rate'))['total_rate'] * 20
        except:
            return None
        return result

    def get_food_quality_average(self):
        try:
            result = self.comments.aggregate(food_quality_average=Avg('food_quality'))['food_quality_average'] * 20
        except:
            return None
        return result

    def get_packaging_quality_average(self):
        try:
            result = self.comments.aggregate(packaging_quality_average=Avg('packaging_quality'))[
                         'packaging_quality_average'] * 20
        except:
            return None
        return result

    def get_speed_transfer_average(self):
        try:
            result = self.comments.aggregate(food_quality_average=Avg('speed_transfer'))['food_quality_average'] * 20
        except:
            return None
        return result


class Category(BaseModel):
    title = models.CharField(max_length=25, verbose_name=_('category'))

    class Meta:
        db_table = 'category'
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.title


class Food(BaseModel):
    title = models.CharField(max_length=250, verbose_name=_('title'))
    price = models.PositiveIntegerField()
    description = models.TextField(verbose_name=_('description'))
    active = models.BooleanField(verbose_name=_('active'), default=True)
    image = models.ImageField(upload_to="images/food/", verbose_name=_('image'))

    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, related_name='foods',
                                   verbose_name=_('restaurant'))
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name=_('category'),
                                 related_name='foods')

    class Meta:
        db_table = 'food'
        verbose_name = _('food')
        verbose_name_plural = _('foods')

    def __str__(self):
        return self.title


class RestaurantCategory(BaseModel):
    restaurant = models.ForeignKey(Restaurant, models.CASCADE, related_name='restaurant_categories')
    category = models.ForeignKey(Category, models.CASCADE, related_name='restaurant_categories')

    class Meta:
        db_table = 'restaurant_category'
