from django.utils.translation import ugettext_lazy as _

################################################
RESTAURANT = 1
COFFEE_SHOP = 2
CONFECTIONERY = 3
SUPER_MARKET = 4
SERVICE_CHOICES = (
    (RESTAURANT, _('Restaurant')),
    (COFFEE_SHOP, _('Coffee shop')),
    (CONFECTIONERY, _('Confectionery')),
    (SUPER_MARKET, _('Super market'))
)
#################################################
TEN_TWENTY = 1
TWENTY_THIRTY = 2
THIRTY_FORTY = 3
FORTY_FIFTY = 4
FIFTY_SIXTY = 5
TIME_TO_DELIVERY_CHOICES = (
    (TEN_TWENTY, _('ten_twenty')),
    (TWENTY_THIRTY, _('twenty_thirty')),
    (THIRTY_FORTY, _('thirty_forty')),
    (FORTY_FIFTY, _('forty_fifty')),
    (FIFTY_SIXTY, _('fifty_sixty')),
)
