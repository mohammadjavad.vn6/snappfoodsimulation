from django.contrib import admin

from restaurant.models import Restaurant, Food, Category


@admin.register(Restaurant)
class RestaurantAdmin(admin.ModelAdmin):
   pass


@admin.register(Food)
class FoodAdmin(admin.ModelAdmin):
    pass


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass
