from django.urls import path
from . import views

app_name = 'comment'
urlpatterns = [
    path('<int:order_id>/', views.CommentFormView.as_view(), name='comment')
]
