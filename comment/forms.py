from django.forms import ModelForm

from comment.models import Comment


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['food_quality', 'packaging_quality', 'speed_transfer', 'agent', 'description']

