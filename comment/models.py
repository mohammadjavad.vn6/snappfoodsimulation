from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.models import BaseModel
from restaurant.models import Food, Restaurant


class Comment(BaseModel):
    VERY_BAD = 1
    BAD = 2
    MEDIUM = 3
    GOOD = 4
    EXCELLENT = 5
    CHOICES = (
        (VERY_BAD, _('very bad')),
        (BAD, _('bad')),
        (MEDIUM, _('medium')),
        (GOOD, _('good')),
        (EXCELLENT, _('excellent'))
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, 'comments',
                             verbose_name=_('user'))
    restaurant = models.ForeignKey(Restaurant, models.CASCADE, 'comments', blank=True)
    food_quality = models.FloatField(choices=CHOICES, verbose_name=_('food_quality'), blank=True)
    packaging_quality = models.FloatField(choices=CHOICES, verbose_name=_('packaging_quality'), blank=True)
    speed_transfer = models.FloatField(choices=CHOICES, verbose_name=_('speed_transfer'), blank=True)
    agent = models.FloatField(choices=CHOICES, verbose_name=_('agent'), blank=True)
    description = models.TextField(verbose_name=_('description'), blank=True)
    active = models.BooleanField(default=False)

    class Meta:
        db_table = 'comment'
        verbose_name = _('comment')
        verbose_name_plural = _('comments')
