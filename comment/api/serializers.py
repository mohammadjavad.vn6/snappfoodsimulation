from rest_framework import serializers


from comment.models import Comment


class CommentProfileUserSerializer(serializers.ModelSerializer):

    restaurant = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = (
            'restaurant', 'food_quality', 'packaging_quality', 'speed_transfer'
            , 'agent', 'description', 'active'
        )

    def get_restaurant(self, obj):
        from restaurant.api.serializers import RestaurantLightSerializer
        comment_restaurant = obj.restaurant
        restaurant = RestaurantLightSerializer(comment_restaurant)
        return restaurant.data


class CommentRestaurantSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = (
            'food_quality', 'packaging_quality', 'speed_transfer',
             'agent', 'description', 'active',
        )


class OrderCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = (
            'food_quality', 'packaging_quality', 'speed_transfer',
            'agent', 'description', 'active',
        )