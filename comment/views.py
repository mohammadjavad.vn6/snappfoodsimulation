from django.urls import reverse_lazy
from django.views.generic import FormView

from comment.forms import CommentForm
from order.models import Order


class CommentFormView(FormView):
    form_class = CommentForm
    template_name = 'comment/comment.html'
    success_url = reverse_lazy('account:profile')

    def form_valid(self, form):
        new_comment = CommentForm.save(form, commit=False)
        new_comment.restaurant = Order.objects.get(id=self.kwargs['order_id']).restaurant
        new_comment.user = self.request.user
        new_comment.save()
        return super().form_valid(form)
